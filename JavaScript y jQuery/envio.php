<?php
	/* Filtrar los datos */
	$nombre = addslashes(htmlspecialchars($_POST["nombre"]));
	$edad = addslashes(htmlspecialchars($_POST["edad"]));
	$nacimiento = addslashes(htmlspecialchars($_POST["nacimiento"]));
	$ciudad = addslashes(htmlspecialchars($_POST["ciudad"]));

	if(empty($nombre) || empty($edad) || empty($nacimiento) || empty($ciudad)) echo "Rellene todos los campos por favor";
	else {	
		echo "Tus datos han sido recogidos con exito!";
		echo "<p>Tu nombre es <b>".$nombre."</b></p>";
		echo "<p>Tu edad es <b>".$edad."</b></p>";
		echo "<p>Tu fecha de nacimiento es <b>".$nacimiento."</b></p>";
		echo "<p>Tu ciudad es <b>".$ciudad."</b></p>";
	}
?>