$(function()
{
 $("#btn_enviar").click(function()
 {
 var url = "envio.php"; // El script a dónde se realizará la petición.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#formulario").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data)
           {
               $("#respuesta").html(data); // Mostrar la respuestas del script PHP.
           }
         });

    return false; // Evitar ejecutar el submit del formulario.
 });
});


function init()
{
  var ciudades =  ["Madrid","Bruselas","Londres","Berlín","Amsterdam","Roma","Granada","Córdoba","París","Viena","Chicago","Lisboa"];
  $("#ciudad").autocomplete({source:ciudades});
  $("#nacimiento").datepicker();
  $("#divEdad").slider({min: 0, max: 100, slide: function(event, ui) { $("#edad").val(ui.value);}});

  //Propiedades CSS
  $("#divEdad").css("width","150px");
  $("#edad").css("width","40px");
  $("#divEdad").css("margin","-20px 0 8px 70px");

  $("#edad").val($("#divEdad").slider("value"));
  $("#edad").change(setSlideValue);
}